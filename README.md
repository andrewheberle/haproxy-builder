# HAproxy Builder

Builds HAproxy from source and packages as a RPM for Centos 7

## Important

This RPM requires Lua 5.3 from https://gitlab.com/andrewheberle/lua-builder

## Versions

### v2.0

* 2.0.4-1 - [![pipeline status](https://gitlab.com/andrewheberle/haproxy-builder/badges/2.0.4-1/pipeline.svg)](https://gitlab.com/andrewheberle/haproxy-builder/commits/2.0.4-1) - [Download](https://gitlab.com/andrewheberle/haproxy-builder/-/jobs/267319196/artifacts/raw/rpmbuild/RPMS/x86_64/haproxy-2.0.4-1.el7.x86_64.rpm)
* 2.0.3-1 - [![pipeline status](https://gitlab.com/andrewheberle/haproxy-builder/badges/2.0.3-1/pipeline.svg)](https://gitlab.com/andrewheberle/haproxy-builder/commits/2.0.3-1) - [Download](https://gitlab.com/andrewheberle/haproxy-builder/-/jobs/257927506/artifacts/raw/rpmbuild/RPMS/x86_64/haproxy-2.0.3-1.el7.x86_64.rpm)
* 2.0.1-1 - [![pipeline status](https://gitlab.com/andrewheberle/haproxy-builder/badges/2.0.1-1/pipeline.svg)](https://gitlab.com/andrewheberle/haproxy-builder/commits/2.0.1-1) - [Download](https://gitlab.com/andrewheberle/haproxy-builder/-/jobs/245187683/artifacts/raw/rpmbuild/RPMS/x86_64/haproxy-2.0.1-1.el7.x86_64.rpm)

### v1.9

* 1.9.8-2 - [![pipeline status](https://gitlab.com/andrewheberle/haproxy-builder/badges/1.9.8-2/pipeline.svg)](https://gitlab.com/andrewheberle/haproxy-builder/commits/1.9.8-2) - [Download](https://gitlab.com/andrewheberle/haproxy-builder/-/jobs/212705006/artifacts/raw/rpmbuild/RPMS/x86_64/haproxy-1.9.8-2.el7.x86_64.rpm)
* 1.9.8-1 - [![pipeline status](https://gitlab.com/andrewheberle/haproxy-builder/badges/1.9.8-1/pipeline.svg)](https://gitlab.com/andrewheberle/haproxy-builder/commits/1.9.8-1) - [Download](https://gitlab.com/andrewheberle/haproxy-builder/-/jobs/212705006/artifacts/raw/rpmbuild/RPMS/x86_64/haproxy-1.9.8-1.el7.x86_64.rpm)
* 1.9.7-2 - [![pipeline status](https://gitlab.com/andrewheberle/haproxy-builder/badges/1.9.7-2/pipeline.svg)](https://gitlab.com/andrewheberle/haproxy-builder/commits/1.9.7-2) - [Download](https://gitlab.com/andrewheberle/haproxy-builder/-/jobs/204663147/artifacts/raw/rpmbuild/RPMS/x86_64/haproxy-1.9.7-2.el7.x86_64.rpm)
* 1.9.7-1 - [![pipeline status](https://gitlab.com/andrewheberle/haproxy-builder/badges/1.9.7-1/pipeline.svg)](https://gitlab.com/andrewheberle/haproxy-builder/commits/1.9.7-1) - [Download](https://gitlab.com/andrewheberle/haproxy-builder/-/jobs/204637760/artifacts/raw/rpmbuild/RPMS/x86_64/haproxy-1.9.7-1.el7.x86_64.rpm)

### v1.8

* 1.8.19-4 - [![pipeline status](https://gitlab.com/andrewheberle/haproxy-builder/badges/1.8.19-4/pipeline.svg)](https://gitlab.com/andrewheberle/haproxy-builder/commits/1.8.19-4) - [Download](https://gitlab.com/andrewheberle/haproxy-builder/-/jobs/204071375/artifacts/raw/rpmbuild/RPMS/x86_64/haproxy-1.8.19-4.el7.x86_64.rpm)
* 1.8.19-3 - [![pipeline status](https://gitlab.com/andrewheberle/haproxy-builder/badges/1.8.19-3/pipeline.svg)](https://gitlab.com/andrewheberle/haproxy-builder/commits/1.8.19-3) - [Download](https://gitlab.com/andrewheberle/haproxy-builder/-/jobs/196532428/artifacts/raw/rpmbuild/RPMS/x86_64/haproxy-1.8.19-3.el7.x86_64.rpm)
* 1.8.19-2 - [![pipeline status](https://gitlab.com/andrewheberle/haproxy-builder/badges/1.8.19-2/pipeline.svg)](https://gitlab.com/andrewheberle/haproxy-builder/commits/1.8.19-2) - [Download](https://gitlab.com/andrewheberle/haproxy-builder/-/jobs/196521243/artifacts/raw/rpmbuild/RPMS/x86_64/haproxy-1.8.19-2.el7.x86_64.rpm)
* 1.8.19-1 - [![pipeline status](https://gitlab.com/andrewheberle/haproxy-builder/badges/1.8.19-1/pipeline.svg)](https://gitlab.com/andrewheberle/haproxy-builder/commits/1.8.19-1) - [Download](https://gitlab.com/andrewheberle/haproxy-builder/-/jobs/196502723/artifacts/raw/rpmbuild/RPMS/x86_64/haproxy-1.8.19-1.el7.x86_64.rpm)
