Name: haproxy
Version: 2.0.4
Release: 1%{?dist}
Source0: http://www.haproxy.org/download/2.0/src/haproxy-%{version}.tar.gz
Summary: TCP/HTTP proxy and load balancer for high availability environments
URL: http://www.haproxy.org/
License: GPLv2+
Group: System Environment/Daemons
Requires: lua53 >= 5.3.5
Requires: openssl
Requires: pcre
Requires: zlib
BuildRequires: lua53 >= 5.3.5
BuildRequires: systemd-devel
BuildRequires: openssl-devel
BuildRequires: pcre-devel
BuildRequires: zlib-devel

%description
HAProxy is a TCP/HTTP reverse proxy which is particularly suited for high
availability environments. Indeed, it can:
 - route HTTP requests depending on statically assigned cookies
 - spread load among several servers while assuring server persistence
   through the use of HTTP cookies
 - switch to backup servers in the event a main server fails
 - accept connections to special ports dedicated to service monitoring
 - stop accepting connections without breaking existing ones
 - add, modify, and delete HTTP headers in both directions
 - block requests matching particular patterns
 - report detailed status to authenticated users from a URI
   intercepted by the application

%prep
%setup -q

%build
make -j 4 TARGET=linux-glibc USE_SYSTEMD=1 USE_PCRE=1 USE_OPENSSL=1 USE_ZLIB=1 USE_LUA=1 LUA_LIB=/opt/lua53/lib LUA_INC=/opt/lua53/include
make -C contrib/systemd PREFIX=%{_prefix}

%install
make PREFIX=%{buildroot}%{_prefix} install
mkdir -p %{buildroot}%{_sysconfdir}/systemd/system
cp -f contrib/systemd/haproxy.service %{buildroot}%{_sysconfdir}/systemd/system
mv %{buildroot}%{_prefix}/doc %{buildroot}%{_docdir}

%files
%{_sbindir}/haproxy
%{_mandir}/man1/*
%{_docdir}/haproxy/*
%{_sysconfdir}/systemd/system/haproxy.service
